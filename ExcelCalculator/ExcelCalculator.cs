﻿using System.Linq;
using ExcelDna.Integration;
using ExcelParser;
using Microsoft.FSharp.Collections;
using Redington.Analytics.ObjectHandler;

namespace ExcelCalculator
{
    public class ExcelCalculator
    {
        [ExcelFunction]
        public static object Calculate(object expression, string handle = null)
        {
            return Parser.Calculate(expression, handle);
        }

        [ExcelFunction(IsMacroType = true)]
        public static object StoreFloatList([ExcelArgument] string handle, [ExcelArgument] object[] listToStore)
        {
            var caller = Handler.NewCaller();

            if (!Handler.IsValidCaller(handle))
            {
                return ExcelError.ExcelErrorValue;
            }

            var fSharpList = ListModule.OfSeq(listToStore.Select(ToString).Select(double.Parse));
            return Handler.StoreObject(handle, caller, fSharpList);
        }

        [ExcelFunction]
        public static object RetrieveFloatList([ExcelArgument] string handle)
        {
            var fSharpList = Handler.RetrieveObject<FSharpList<double>>(handle);
            return SheetFunctions.DisplayList(fSharpList.ToArray());
        }

        private static string ToString(object o)
        {
            return o.ToString();
        }
    }
}
