﻿namespace ExcelParserTests

module ShuntingYardTests = 
    open ExcelParser.Operators
    open ExcelParser.Parser
    open ExcelParser.ExpressionParser
    open ExcelParser.ShuntingYard
    open NUnit.Framework
    open Swensen.Unquote
    open System
    
    [<Test>]
    let ``InfixToPostfix 1 + 2``() = 
        InfixToPostfix [ Val (float 1)
                         Operator Operator.Add
                         Val (float 2) ]
        =! PostFixList([ Val (float 1)
                         Val (float 2)
                         Operator Operator.Add ])
    
    [<Test>]
    let ``InfixToPostfix 1 + 2 * 3``() = 
        InfixToPostfix [ Val (float 1)
                         Operator Operator.Add
                         Val (float 2)
                         Operator Operator.Multiply
                         Val (float 3) ]
        =! PostFixList([ Val (float 1)
                         Val (float 2)
                         Val (float 3)
                         Operator Operator.Multiply
                         Operator Operator.Add ])
    
    [<Test>]
    let ``InfixToPostfix 1 * 2 + 3``() = 
        InfixToPostfix [ Val (float 1)
                         Operator Operator.Multiply
                         Val (float 2)
                         Operator Operator.Add
                         Val (float 3) ]
        =! PostFixList([ Val (float 1)
                         Val (float 2)
                         Operator Operator.Multiply
                         Val (float 3)
                         Operator Operator.Add ])
    
    [<Test>]
    let ``InfixToPostfix (1 + 2) * 3``() = 
        InfixToPostfix [ LeftBracket
                         Val (float 1)
                         Operator Operator.Add
                         Val (float 2)
                         RightBracket
                         Operator Operator.Multiply
                         Val (float 3) ]
        =! PostFixList([ Val (float 1)
                         Val (float 2)
                         Operator Operator.Add
                         Val (float 3)
                         Operator Operator.Multiply ])
    
    [<Test>]
    let ``InfixToPostfix 1 ^ 2 ^ 3``() = 
        InfixToPostfix [ Val (float 1)
                         Operator Operator.Exponent
                         Val (float 2)
                         Operator Operator.Exponent
                         Val (float 3) ]
        =! PostFixList([ Val (float 1)
                         Val (float 2)
                         Val (float 3)
                         Operator Operator.Exponent
                         Operator Operator.Exponent ])
    
    [<Test>]
    let ``InfixToPostfix 5 + ((1 + 2) * 4) - 3``() = 
        InfixToPostfix [ Val (float 5)
                         Operator Operator.Add
                         LeftBracket
                         LeftBracket
                         Val (float 1)
                         Operator Operator.Add
                         Val (float 2)
                         RightBracket
                         Operator Operator.Multiply
                         Val (float 4)
                         RightBracket
                         Operator Operator.Subtract
                         Val (float 3) ]
        =! PostFixList([ Val (float 5)
                         Val (float 1)
                         Val (float 2)
                         Operator Operator.Add
                         Val (float 4)
                         Operator Operator.Multiply
                         Operator Operator.Add
                         Val (float 3)
                         Operator Operator.Subtract ])

    [<Test>]
    let ``InfixToPostfix A1 + B2``() = 
        InfixToPostfix [ Ref { col = 1
                               row = 1 }
                         Operator Operator.Add
                         Ref { col = 2
                               row = 2 } ]
        =! PostFixList([ Ref { col = 1
                               row = 1 }
                         Ref { col = 2
                               row = 2 }
                         Operator Operator.Add ])