﻿namespace ExcelParserTests

module RPNCalculatorTests = 
    open ExcelParser.Operators
    open ExcelParser.Parser
    open ExcelParser.ExpressionParser
    open ExcelParser.ShuntingYard
    open ExcelParser.RPNCalculator
    open NUnit.Framework
    open Swensen.Unquote
    open System
    open Redington.Analytics.ObjectHandler
    
    [<Test>]
    let ``Calculate 1 + 2``() = 
        Calculate(PostFixList([ Val(float 1)
                                Val(float 2)
                                Operator Operator.Add ]))
        =! ValResult(float 3)
    
    [<Test>]
    let ``Calculate 5 + ((1 + 2) * 4) - 3``() = 
        Calculate(PostFixList([ Val(float 5)
                                Val(float 1)
                                Val(float 2)
                                Operator Operator.Add
                                Val(float 4)
                                Operator Operator.Multiply
                                Operator Operator.Add
                                Val(float 3)
                                Operator Operator.Subtract ]))
        =! ValResult(float 14)
    
    [<Test>]
    let ``Calculate A1 + B2``() = 
        Calculate(PostFixList([ Ref { col = 1
                                      row = 1 }
                                Ref { col = 2
                                      row = 2 }
                                Operator Operator.Add ]))
        =! ValResult(float 3)
    
    [<Test>]
    let ``Calculate with handle``() = 
        let ls = [ 1.0; 2.0; 3.0; 4.0 ]
        Handler.ActiveHandler <- new TestHandler()
        let lsHandle = Handler.StoreObject("Test", Handler.NewCaller(), ls)
        Calculate(PostFixList([ Handle lsHandle
                                Val(float 2)
                                Operator Operator.Add ]))
        =! ValListResult [ 3.0; 4.0; 5.0; 6.0 ]
