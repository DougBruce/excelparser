﻿namespace ExcelParser

module OperatorFunctionsTests = 
    open ExcelParser.Operators
    open ExcelParser.OperatorFunctions
    open ExcelParser.ExpressionParser
    open NUnit.Framework
    open Swensen.Unquote
    open System
    
    [<Test>]
    let ``Add x y works on values``() = Add (Val 1.0) (Val 2.0) =! Val 3.0
    
    [<Test>]
    let ``Add x y works on a value and a list``() = Add (ValList [ 1.0; 1.5 ]) (Val 2.0) =! ValList [ 3.0; 3.5 ]
    
    [<Test>]
    let ``Add x y works on a list and a value``() = Add (Val 2.0) (ValList [ 1.0; 1.5 ]) =! ValList [ 3.0; 3.5 ]
    
    [<Test>]
    let ``Add x y works on two lists``() = Add (ValList [ 1.0; 1.5 ]) (ValList [ 1.0; 1.5 ]) =! ValList [ 2.0; 3.0 ]
    
    [<Test>]
    let ``Subtract x y works on values``() = Subtract (Val 1.0) (Val 2.0) =! Val -1.0
    
    [<Test>]
    let ``Subtract x y works on a value and a list``() = 
        Subtract (ValList [ 1.0; 1.5 ]) (Val 2.0) =! ValList [ -1.0; -0.5 ]
    
    [<Test>]
    let ``Subtract x y works on a list and a value``() = 
        Subtract (Val 2.0) (ValList [ 1.0; 1.5 ]) =! ValList [ 1.0; 0.5 ]
    
    [<Test>]
    let ``Subtract x y works on two lists``() = 
        Subtract (ValList [ 1.0; 1.5 ]) (ValList [ 1.0; 1.5 ]) =! ValList [ 0.0; 0.0 ]
    
    [<Test>]
    let ``Multiply x y works on values``() = Multiply (Val 1.0) (Val 2.0) =! Val 2.0
    
    [<Test>]
    let ``Multiply x y works on a value and a list``() = 
        Multiply (ValList [ 1.0; 1.5 ]) (Val 2.0) =! ValList [ 2.0; 3.0 ]
    
    [<Test>]
    let ``Multiply x y works on a list and a value``() = 
        Multiply (Val 2.0) (ValList [ 1.0; 1.5 ]) =! ValList [ 2.0; 3.0 ]
    
    [<Test>]
    let ``Multiply x y works on two lists``() = 
        Multiply (ValList [ 1.0; 1.5 ]) (ValList [ 1.0; 1.5 ]) =! ValList [ 1.0; 2.25 ]
    
    [<Test>]
    let ``Divide x y works on values``() = Divide (Val 1.0) (Val 2.0) =! Val 0.5
    
    [<Test>]
    let ``Divide x y works on a value and a list``() = Divide (ValList [ 1.0; 1.5 ]) (Val 2.0) =! ValList [ 0.5; 0.75 ]
    
    [<Test>]
    let ``Divide x y works on a list and a value``() = 
        Divide (Val 2.0) (ValList [ 1.0; 1.5 ]) =! ValList [ 2.0
                                                             4.0 / 3.0 ]
    
    [<Test>]
    let ``Divide x y works on two lists``() = 
        Divide (ValList [ 1.0; 1.5 ]) (ValList [ 1.0; 1.5 ]) =! ValList [ 1.0; 1.0 ]
    
    [<Test>]
    let ``Exponent x y works on values``() = Exponent (Val 1.0) (Val 2.0) =! Val 1.0
    
    [<Test>]
    let ``Exponent x y works on a value and a list``() = 
        Exponent (ValList [ 1.0; 1.5 ]) (Val 2.0) =! ValList [ 1.0; 2.25 ]
    
    [<Test>]
    let ``Exponent x y works on a list and a value``() = 
        Exponent (Val 2.0) (ValList [ 1.0; 1.5 ]) =! ValList [ 2.0
                                                               2.0 ** 1.5 ]
    
    [<Test>]
    let ``Exponent x y works on two lists``() = 
        Exponent (ValList [ 1.0; 1.5 ]) (ValList [ 1.0; 1.5 ]) =! ValList [ 1.0
                                                                            1.5 ** 1.5 ]
