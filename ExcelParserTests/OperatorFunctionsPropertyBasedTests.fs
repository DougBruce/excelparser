﻿namespace ExcelParser

module OperatorFunctionsPropertyBasedTests = 
    open ExcelParser.Operators
    open ExcelParser.OperatorFunctions
    open ExcelParser.ExpressionParser
    open NUnit.Framework
    
    let rand = System.Random()
    let randInt() = rand.Next()
    
    let propertyCheck (property : Token -> Token -> bool) = 
        for _ in [ 1..100 ] do
            let x = Val (float (randInt()))
            let y = Val (float (randInt()))
            let result = property x y
            Assert.IsTrue(result)
    
    let commutativeProperty x y = 
        let result1 = Add x y
        let result2 = Add y x
        result1 = result2
    
    [<Test>]
    let ``When I add two numbers, the result should not depend on parameter order``() = 
        propertyCheck commutativeProperty
    
    let adding1TwiceIsAdding2OnceProperty x _ = 
        let result1 = 
            x
            |> Add (Val(1.0))
            |> Add (Val(1.0))
        
        let result2 = x |> Add (Val(2.0))
        result1 = result2
    
    [<Test>]
    let ``Adding 1 twice is the same as adding 2``() = propertyCheck adding1TwiceIsAdding2OnceProperty
    
    let identityProperty x _ = 
        let result1 = x |> Add (Val(0.0))
        result1 = x
    
    [<Test>]
    let ``Adding zero is the same as doing nothing``() = propertyCheck identityProperty
