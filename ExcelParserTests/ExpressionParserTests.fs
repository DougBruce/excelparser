﻿namespace ExcelParserTests

module ExpressionParserTests = 
    open ExcelParser.Operators
    open ExcelParser.Parser
    open ExcelParser.ExpressionParser
    open NUnit.Framework
    open Swensen.Unquote
    open System
    
    [<Test>]
    let ``IsRefPattern A1 returns true``() = IsRefPattern "A1" =! true
    
    [<Test>]
    let ``IsRefPattern AA11 returns true``() = IsRefPattern "AA11" =! true
    
    [<Test>]
    let ``IsRefPattern 1A1 returns false``() = IsRefPattern "1A1" =! false
    
    [<Test>]
    let ``IsRefPattern A1A returns false``() = IsRefPattern "A1A" =! false
    
    [<Test>]
    let ``IsRefPattern $A$1 returns true``() = IsRefPattern "$A$1" =! true
    
    [<Test>]
    let ``IsRefPattern A$1 returns true``() = IsRefPattern "A$1" =! true
    
    [<Test>]
    let ``IsRefPattern $A1 returns true``() = IsRefPattern "$A1" =! true
    
    [<Test>]
    let ``IsRefPattern $$A1 returns false``() = IsRefPattern "$$A1" =! false
    
    [<Test>]
    let ``IsRefPattern A$$1 returns false``() = IsRefPattern "A$$1" =! false
    
    [<Test>]
    let ``LetterIndexToNumberIndex A = 1``() = LetterIndexToNumberIndex "A" =! 1
    
    [<Test>]
    let ``LetterIndexToNumberIndex Z = 26``() = LetterIndexToNumberIndex "Z" =! 26
    
    [<Test>]
    let ``LetterIndexToNumberIndex AA = 27``() = LetterIndexToNumberIndex "AA" =! 27

    [<Test>]
    let ``LetterIndexToNumberIndex AB = 28``() = LetterIndexToNumberIndex "AB" =! 28
    
    [<Test>]
    let ``LetterIndexToNumberIndex BB = 54``() = LetterIndexToNumberIndex "BB" =! 54
    
    [<Test>]
    let ``LetterIndexToNumberIndex ZZ = 702``() = LetterIndexToNumberIndex "ZZ" =! 702
    
    [<Test>]
    let ``RefFromString A1``() = 
        RefFromString "A1" =! Ref { col = 1
                                    row = 1 }
    
    [<Test>]
    let ``RefFromString $A1``() = 
        RefFromString "$A1" =! Ref { col = 1
                                     row = 1 }

    [<Test>]
    let ``RefFromString $A$1``() = 
        RefFromString "$A$1" =! Ref { col = 1
                                      row = 1 }

    [<Test>]
    let ``RefFromString A$1``() = 
        RefFromString "A$1" =! Ref { col = 1
                                     row = 1 }
                                                                              
    [<Test>]
    let ``RefFromString AA1``() = 
        RefFromString "AA1" =! Ref { col = 27
                                     row = 1 }
    
    [<Test>]
    let ``RefFromString AA11``() = 
        RefFromString "AA11" =! Ref { col = 27
                                      row = 11 }
    
    [<Test>]
    let ``RefFromString AA111``() = 
        RefFromString "AA111" =! Ref { col = 27
                                       row = 111 }
    
    [<Test>]
    let ``StringToTokenList 1+2``() = 
        StringToTokenList "1+2" =! [ Val(float 1)
                                     Operator Operator.Add
                                     Val(float 2) ]
    
    [<Test>]
    let ``StringToTokenList 1*2``() = 
        StringToTokenList "1*2" =! [ Val(float 1)
                                     Operator Operator.Multiply
                                     Val(float 2) ]
    
    [<Test>]
    let ``StringToTokenList 1-2``() = 
        StringToTokenList "1-2" =! [ Val(float 1)
                                     Operator Operator.Subtract
                                     Val(float 2) ]
    
    [<Test>]
    let ``StringToTokenList 1/2``() = 
        StringToTokenList "1/2" =! [ Val(float 1)
                                     Operator Operator.Divide
                                     Val(float 2) ]
    
    [<Test>]
    let ``StringToTokenList 1^2``() = 
        StringToTokenList "1^2" =! [ Val(float 1)
                                     Operator Operator.Exponent
                                     Val(float 2) ]
    
    [<Test>]
    let ``StringToTokenList whitespace``() = 
        StringToTokenList " 1 + 2 " =! [ Val(float 1)
                                         Operator Operator.Add
                                         Val(float 2) ]
    
    [<Test>]
    let ``StringToTokenList brackets``() = 
        StringToTokenList "(1 + 2)" =! [ LeftBracket
                                         Val(float 1)
                                         Operator Operator.Add
                                         Val(float 2)
                                         RightBracket ]
    
    [<Test>]
    let ``StringToTokenList brackets and whitespace``() = 
        StringToTokenList "( 1 + 2 )" =! [ LeftBracket
                                           Val(float 1)
                                           Operator Operator.Add
                                           Val(float 2)
                                           RightBracket ]
    
    [<Test>]
    let ``StringToTokenList multi digit integers``() = 
        StringToTokenList "( 123 + 321 )" =! [ LeftBracket
                                               Val(float 123)
                                               Operator Operator.Add
                                               Val(float 321)
                                               RightBracket ]
    
    [<Test>]
    let ``StringToTokenList floats``() = 
        StringToTokenList "1.23 + 3.21" =! [ Val 1.23
                                             Operator Operator.Add
                                             Val 3.21 ]
    
    [<Test>]
    let ``StringToTokenList reference A1``() = 
        StringToTokenList "A1" =! [ Ref { col = 1
                                          row = 1 } ]
    
    [<Test>]
    let ``StringToTokenList A1 + A2 * BB312``() = 
        StringToTokenList "A1 + A2 * BB312" =! [ Ref { col = 1
                                                       row = 1 }
                                                 Operator Operator.Add
                                                 Ref { col = 1
                                                       row = 2 }
                                                 Operator Operator.Multiply
                                                 Ref { col = 54
                                                       row = 312 } ]

    [<Test>]
    let ``StringToTokenList handle``() = 
        StringToTokenList "Test" =! [ Handle "Test" ]
