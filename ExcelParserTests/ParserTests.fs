﻿namespace ExcelParserTests

module ParserTests = 
    open ExcelParser.Operators
    open ExcelParser.Parser
    open ExcelParser.ExpressionParser
    open ExcelParser.ShuntingYard
    open ExcelParser.RPNCalculator
    open NUnit.Framework
    open Swensen.Unquote
    open System
    
    [<Test>]
    let ``GetFormulaFromText returns formula body``() = GetFormulaFromText "=Calculate(formula)" =! "formula" 
    
    [<Test>]
    let ``GetFormulaFromText with handle returns formula body``() = GetFormulaFromText "=Calculate(formula,handle)" =! "formula"   
    
    [<Test>]
    let ``GetFormulaFromText with handle and space returns formula body``() = GetFormulaFromText "=Calculate(formula, handle)" =! "formula"
    
    [<Test>]
    let ``CalcFormulaFromText 1 + 2``() = CalcFormulaFromText "1 + 2" =! ValResult (float 3)