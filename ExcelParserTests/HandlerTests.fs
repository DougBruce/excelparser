﻿namespace ExcelParserTests

module HandlerTests = 
    open Redington.Analytics.ObjectHandler
    open NUnit.Framework
    open Swensen.Unquote
    open System

    [<Test>]
    let ``Handler can store & retrieve a list``() = 
      let ls = [1;2;3;4]
      Handler.ActiveHandler <- new TestHandler()
      let lsHandle = Handler.StoreObject("Test", Handler.NewCaller(), ls);
      let lsReturned = Handler.RetrieveObject<int List>(lsHandle)
      lsReturned =! ls
