﻿namespace ExcelParserTests

module ManhattanDistance = 
    open NUnit.Framework
    open Swensen.Unquote
    open System.Diagnostics
    
    let manhattanDistance1 (pixels1, pixels2 : int []) = 
        Array.zip pixels1 pixels2
        |> Array.map (fun (x, y) -> abs (x - y))
        |> Array.sum

    let manhattanDistance2 (pixels1, pixels2 : int []) = 
        Array.zip pixels1 pixels2
        |> Array.fold (fun z (x1, y1) -> abs(x1 - y1) + z) 0


    let rnd = System.Random()
    let numPixels = 10000000

    let pixels1 = seq { for _ in 0 .. numPixels -> rnd.Next(255) } |> Seq.toArray
    let pixels2 = seq { for _ in 0 .. numPixels -> rnd.Next(255) } |> Seq.toArray

    [<Test>]
    let ``test first``() = 
        let stopWatch = Stopwatch.StartNew()
        manhattanDistance1(pixels1, pixels2) |> ignore
        stopWatch.Stop()
        stopWatch.Elapsed.TotalMilliseconds =! 0.0

    [<Test>]
    let ``test second``() = 
        let stopWatch = Stopwatch.StartNew()
        manhattanDistance2(pixels1, pixels2) |> ignore
        stopWatch.Stop()
        stopWatch.Elapsed.TotalMilliseconds  =! 0.0
