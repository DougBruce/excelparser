﻿namespace ExcelParser

module ExpressionParser = 
    open Microsoft.FSharp.Core.LanguagePrimitives
    open System
    open System.Collections.Generic
    open System.Text.RegularExpressions
    open Operators
    
    type Ref = 
        { col : int
          row : int }
    
    type Token = 
        | Operator of Operator
        | LeftBracket
        | RightBracket
        | ValList of float list
        | Val of float
        | Ref of Ref
        | Handle of string
    
    let isOperator char = Enum.IsDefined(typeof<Operator>, char)
    let asOperator = EnumOfValue<char, Operator>
    
    let (|Op|Space|LBracket|RBracket|Other|) input = 
        if isOperator input then Op
        else if input = ' ' then Space
        else if input = '(' then LBracket
        else if input = ')' then RBracket
        else Other
    
    let CharToIntValue char = (int char) - (int 'A') + 1
    
    let LetterIndexToNumberIndex(str : string) = 
        let x = str.ToCharArray()
        x
        |> Array.map CharToIntValue
        |> Array.mapi (fun i elt -> elt * pown 26 (x.Length - i - 1))
        |> Array.sum
    
    let RefFromString str = 
        let numPattern = @"[0-9]+$"
        let firstNumber = Regex.Match(str, numPattern).Index
        let firstLetter = if str.StartsWith("$") then 1 else 0
        let numLetters = if str.[firstNumber - 1] = '$' then firstNumber - firstLetter - 1 else firstNumber - firstLetter
        let col = LetterIndexToNumberIndex(str.Substring(firstLetter, numLetters))
        let row = Int32.Parse(str.Substring(firstNumber))
        Ref { col = col
              row = row }

    let (|IsVal|_|) str = 
        match Int32.TryParse(str) with
        | (true, int) -> Some(float int)
        | _ -> 
            match Double.TryParse(str) with
            | (true, float) -> Some(float)
            | _ -> None
    
    let IsRefPattern str = 
        let refPattern = @"^[$]?[a-zA-Z]+[$]?[0-9]+$"
        let isRef = Regex.Match(str, refPattern)
        isRef.Success

    let (|IsRef|_|) str = 
        match IsRefPattern str with
        | true -> Some(RefFromString str)
        | false -> None
   
    let IsHandlePattern str = true

    let (|IsHandle|_|) str = 
        match IsHandlePattern str with
        | true -> Some(str)
        | false -> None
    
    let readToken (ts : char list) = 
        match ts with
        | [] -> None
        | _ -> 
            let token = String.Concat(Array.ofList (List.rev ts))
            match token with
            | IsVal v -> Some(Val v)
            | IsRef r -> Some r
            | IsHandle h -> Some(Handle h)
            | _ -> None
    
    let StringToTokenList(str : string) = 
        let rec handleOp op tail currentToken tokens = 
            let token = readToken currentToken
            match token with
            | None -> readChars tail (op :: tokens) List.empty
            | Some token -> readChars tail (op :: token :: tokens) List.empty
        
        and readChars chars tokens currentToken = 
            match chars with
            | [] -> 
                let token = readToken currentToken
                match token with
                | None -> tokens
                | Some token -> token :: tokens
            | h :: t -> 
                match h with
                | Op -> handleOp (Operator(asOperator h)) t currentToken tokens
                | Space -> readChars t tokens currentToken
                | LBracket -> handleOp LeftBracket t currentToken tokens
                | RBracket -> handleOp RightBracket t currentToken tokens
                | Other -> readChars t tokens (h :: currentToken)
        
        List.rev (readChars (Array.toList (str.ToCharArray())) List.empty List.empty)
