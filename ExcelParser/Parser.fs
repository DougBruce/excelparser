﻿namespace ExcelParser

module Parser = 
    open ExcelParser.ExpressionParser
    open ExcelParser.ShuntingYard
    open ExcelParser.RPNCalculator
    open ExcelParser.SheetFunctions
    open Microsoft.Office.Interop.Excel
    open Redington.Analytics.ObjectHandler
    open System
    
    let functionName = "Calculate"

    let indexOfComma (text : string) = text.IndexOf ','

    let startOfSubstring = functionName.Length + 2;

    let endOfSubstring text = 
        match indexOfComma text with
        | -1 -> startOfSubstring + 1
        | i -> text.Length - i + startOfSubstring
    
    let GetFormulaFromText(text : string) = 
        text.Substring(startOfSubstring, text.Length - endOfSubstring text)

    let GetReturnValue handle result  = 
        match result with
        | ValResult v -> v :> obj
        | HandleResult h -> h :> obj
        | ValListResult vs -> Handler.StoreObject(handle, new InternalCaller(), vs) :> obj
    
    let GetFormulaFromCell expr = 
        expr
        |> GetCellText
        |> GetFormulaFromText
    
    let Calculate expr handle = 
        expr
        |> GetFormulaFromCell
        |> CalcFormulaFromText
        |> GetReturnValue handle