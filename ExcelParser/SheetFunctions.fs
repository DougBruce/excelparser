﻿namespace ExcelParser

module SheetFunctions = 
    open ExcelDna.Integration
    open ExcelParser.ExpressionParser
    open ExcelParser.ShuntingYard
    open Microsoft.Office.Interop.Excel
    open Microsoft.FSharp.Collections
    
    type MyWS = 
        { A1Val : float
          B2Val : float }
    
    type workSheet = 
        | Worksheet of Worksheet
        | MyWS of MyWS
    
    let TextFromSheet rangeToString (sheet : workSheet) row col = 
        match sheet with
        | Worksheet w -> 
            let range = w.Cells.[row, col] :?> Range
            let cellText = rangeToString range
            cellText
        | MyWS ws -> 
            match (row, col) with
            | (1, 1) -> ws.A1Val.ToString()
            | (2, 2) -> ws.B2Val.ToString()
            | _ -> "0.0"

    let FormulaToString (range : Range) = range.Formula.ToString()
    let FormulaText = TextFromSheet FormulaToString
    
    let GetSheet t1 = 
        let application = ExcelDnaUtil.Application :?> Application
        
        let sheet = 
            if application = null then 
                MyWS { A1Val = 1.0
                       B2Val = 2.0 }
            else Worksheet (application.ActiveWorkbook.ActiveSheet :?> Worksheet)
        sheet
    
    let GetCellText t1 = 
        let callerRef = XlCall.Excel(XlCall.xlfCaller) :?> ExcelReference
        FormulaText (GetSheet t1) (callerRef.RowFirst + 1) (callerRef.ColumnFirst + 1)

    let DisplayList (ls : float[]) = 
        Array2D.init ls.Length 1 (fun x y -> ls.[x] :> obj)
       
