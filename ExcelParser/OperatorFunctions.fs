﻿namespace ExcelParser

module OperatorFunctions = 
    open ExpressionParser
    open Redington.Analytics.ObjectHandler

    exception InvalidInput

    let EvaluateHandleAsFloatList(h : string) = Handler.RetrieveObject<float list>(h)

    let rec ApplyInfixOperator op x y  =         
        match (x, y) with
        | (Val xv, Val yv) -> Val(xv |> op <| yv)
        | (ValList xvs, Val yv) -> ValList(List.map (fun elt -> elt |> op <| yv) xvs)
        | (Val xv, ValList yvs) -> ValList(List.map (fun elt -> xv |> op <| elt) yvs)
        | (ValList xvs, ValList yvs) -> ValList(List.map2 (fun x y -> x |> op <| y) xvs yvs)
        | (Handle xh, Handle yh) -> ApplyInfixOperator op (ValList(EvaluateHandleAsFloatList xh)) (ValList(EvaluateHandleAsFloatList yh)) 
        | (Handle xh, _) -> ApplyInfixOperator op (ValList(EvaluateHandleAsFloatList xh)) y 
        | (_, Handle yh) -> ApplyInfixOperator op x (ValList(EvaluateHandleAsFloatList yh)) 
        | _ -> raise InvalidInput

    let Add = ApplyInfixOperator (+)

    let Subtract = ApplyInfixOperator (-)

    let Multiply  = ApplyInfixOperator  (*)

    let Divide = ApplyInfixOperator (/)

    let Exponent = ApplyInfixOperator ( ** )