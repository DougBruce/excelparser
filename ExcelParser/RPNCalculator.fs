﻿namespace ExcelParser

module RPNCalculator = 
    open System
    open Operators
    open ShuntingYard
    open ExpressionParser
    open SheetFunctions
    open OperatorFunctions
    open Microsoft.Office.Interop.Excel
    open Redington.Analytics.ObjectHandler
    
    type Result = 
        | ValResult of float
        | HandleResult of string
        | ValListResult of float list

    let TextToString (range : Range) = range.Text.ToString()
    let CellText = TextFromSheet TextToString
    
    let GetResult x = 
        match x with
        | ValResult v -> Val v
        | HandleResult h -> Handle h
        | ValListResult vs -> ValList vs
    
    let GetFloatValue x = 
        match x with
        | ValResult v -> v
        | _ -> 0.0
    
    let GetReferenceFromText(text : string) = 
        let cleanText = Handler.GetCleanHandle(text)
        match cleanText.StartsWith("=") with
        | true -> cleanText.Substring(1)
        | false -> cleanText
    
    let rec CalcFormulaFromText = 
        StringToTokenList
        >> InfixToPostfix
        >> Calculate
    
    and EvaluateReference(r : Ref) = 
        CellText (GetSheet 0) (r.row) (r.col)
        |> GetReferenceFromText
        |> CalcFormulaFromText
        |> GetResult
    
    and Eval(PostFixList ls) = 
        let solve items current = 
            match (current, items) with
            | Handle h, _ -> ValList(EvaluateHandleAsFloatList h) :: items
            | Ref r, _ -> (EvaluateReference r) :: items
            | Operator Operator.Add, y :: x :: t -> Add x y :: t
            | Operator Operator.Subtract, y :: x :: t -> Subtract x y :: t
            | Operator Operator.Multiply, y :: x :: t -> Multiply x y :: t
            | Operator Operator.Divide, y :: x :: t -> Divide x y :: t
            | Operator Operator.Exponent, y :: x :: t -> Exponent x y :: t
            | _ -> current :: items
        (ls |> List.fold solve []).Head
    
    and Calculate ls = 
        match Eval ls with
        | Val v -> ValResult v
        | ValList vs -> ValListResult vs
        | Handle h -> HandleResult h
        | _ -> ValResult 0.0
