﻿namespace ExcelParser

module Operators = 

    type Operator = 
        | Exponent = '^'
        | Divide = '/'
        | Multiply = '*'
        | Add = '+'
        | Subtract = '-'
    
    let precedence op = 
        match op with
        | Operator.Exponent -> 4
        | Operator.Multiply | Operator.Divide -> 3
        | Operator.Subtract | Operator.Add -> 2
        | _ -> 0
    
    type associativity = 
        | Left
        | Right
    
    let associativity op = 
        match op with
        | Operator.Exponent -> Right
        | _ -> Left
