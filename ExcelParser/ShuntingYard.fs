﻿namespace ExcelParser

module ShuntingYard = 
    open Operators
    open ExpressionParser

    type PostFixList = PostFixList of Token list
    
    let InfixToPostfix ts = 
        let rec handleOperator operator tokens opStack outQueue = 
            match opStack with // add brackets to the Operator type?
            | [] -> readTokens tokens (Operator operator :: opStack) outQueue
            | Operator head :: tail -> 
                match associativity operator with
                | Left -> 
                    if precedence operator <= precedence head then 
                        handleOperator operator tokens tail (outQueue @ [ Operator head ])
                    else readTokens tokens (Operator operator :: opStack) outQueue
                | Right -> 
                    if precedence operator < precedence head then 
                        handleOperator operator tokens tail (outQueue @ [ Operator head ])
                    else readTokens tokens (Operator operator :: opStack) outQueue
            | _ -> readTokens tokens (Operator operator :: opStack) outQueue
        
        and transferOperators opStack outQueue = 
            match opStack with
            | [] -> outQueue
            | head :: tail -> 
                match head with
                // | leftBracket -> raise an exception
                // |  rightBracket -> raise an exception
                | _ -> transferOperators tail (outQueue @ [ head ])
        
        and handleBrackets tokens opStack outQueue = 
            match opStack with
            // | [] -> raise an exception
            | [] -> []
            | LeftBracket :: t -> readTokens tokens t outQueue
            | h :: t -> handleBrackets tokens t (outQueue @ [ h ])
        
        and readTokens tokens opStack outQueue = 
            match tokens with
            | head :: tail -> 
                match head with
                | Operator op -> handleOperator op tail opStack outQueue
                | LeftBracket -> readTokens tail (head :: opStack) outQueue
                | RightBracket -> handleBrackets tail opStack outQueue
                | _ -> readTokens tail opStack (outQueue @ [ head ])
            | [] -> transferOperators opStack outQueue
        
        PostFixList (readTokens ts List.empty List.empty)
